import actionTypes from '../constants';

// eslint-disable-next-line import/prefer-default-export
export function settingsSet(setting) {
  return {
    type: actionTypes.SETTINGS_SET,
    payload: setting,
  };
}
