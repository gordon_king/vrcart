import _get from 'lodash/get';
import actionTypes from '../constants';

export function mutationSet(set) {
  return {
    type: actionTypes.SETTINGS_SET,
    payload: {
      mutating: set,
    },
  };
}

export function mutationSuccess() {
  return function mutationSuccessDispatch(dispatch) {
    dispatch(mutationSet(false));
  };
}

export function mutationFailure(err) {
  return function mutationFailureDispatch(dispatch) {
    dispatch(mutationSet(false));
    const apiMessage = _get(err, ['graphQLErrors', '0', 'data', 'apiMessage']);
    const message = _get(err, ['graphQLErrors', '0', 'message']);
    if (window.$NODE_ENV === 'development') {
      if (!(message || apiMessage)) {
        // eslint-disable-next-line no-console
        console.error('[MutationFailure error]', JSON.stringify(err, undefined, 2));
      }
    }
  };
}
