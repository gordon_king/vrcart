import settingsReducer, { initialState as settingsInitialState } from './settings_reducer';

export const reducers = {
  settings: settingsReducer,
};

export const initialState = {
  settings: settingsInitialState,
};
