import actionTypes from '../constants';

export const initialState = {
  mutating: false,
  orderId: 1,
};

function rehydrate(state, payload) {
  return payload
    && Object.keys(payload).length > 0
    && Object.keys(payload).includes('settings') ? { ...initialState, ...payload.settings } : initialState;
}

export default function settingsReducer(state = initialState, action) {
  const { type, payload, error } = action;
  if (error) {
    return state;
  }

  switch (type) {
    case actionTypes.SETTINGS_SET:
      return Object.assign({}, state, payload);
    case 'persist/REHYDRATE':
      return rehydrate(state, payload);
    default:
      return state;
  }
}
