import mirrorCreator from 'mirror-creator';

const actionTypes = mirrorCreator([
  'SETTINGS_SET',
  'MUTATION_CREATE',
  'MUTATION_UPDATE',
  'MUTATION_DESTROY',
]);

export default actionTypes;
