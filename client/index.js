// import React from 'react'
// import ReactDOM from 'react-dom'

// const App = (props) => {
//   return <p>Hello azure</p>
// }

// const rootEl = document.getElementById('root')

// function render (Component) {
//   ReactDOM.render(
//     <App />,
//     rootEl
//   )
// }
// render(App)
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';
import ApolloClient from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { createHttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from 'react-apollo';

import { Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';

import { PersistGate } from 'redux-persist/es/integration/react';

import Loader from './components/loader';
import { store, history, persistor } from './store';
import App from './containers/app';

// if (!process.env.APP_URL) {
//   throw new Error('Please define APP_URL');
// }

const httpLink = createHttpLink({ uri: '/graphql' });

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (window.$APP_ENV !== 'production') {
    if (graphQLErrors) {
      // eslint-disable-next-line no-console
      graphQLErrors.map(graphQLError => console.log('[GraphQL error]', JSON.stringify(graphQLError, undefined, 2)));
    }
    if (networkError) {
      // eslint-disable-next-line no-console
      console.log('[Network error]', JSON.stringify(networkError, undefined, 2));
    }
  }
});

const link = ApolloLink.from([errorLink, httpLink]);

const cache = new InMemoryCache({
  addTypename: true,
  dataIdFromObject: (o) => {
    const id = o.id ? o.id : o.Id;
    // eslint-disable-next-line no-underscore-dangle
    return `${o.__typename}-${id}`;
  },
});

const apolloClient = new ApolloClient({
  link,
  // eslint-disable-next-line no-underscore-dangle
  cache: cache.restore(window.__APOLLO_STATE__ || {}),
  queryDeduplication: true,
  connectToDevTools: process.env.NODE_ENV !== 'production',
});

const rootEl = document.getElementById('root');

const renderApp = Component => (
  <AppContainer>
    <Provider store={store}>
      <PersistGate
        loading={<Loader />}
        persistor={persistor}
      >
        <ApolloProvider client={apolloClient}>
          <ConnectedRouter history={history}>
            <Route path="/" component={Component} />
          </ConnectedRouter>
        </ApolloProvider>
      </PersistGate>
    </Provider>
  </AppContainer>
);

render(renderApp(App), rootEl);

if (module.hot) {
  module.hot.accept('./containers/app', () => {
    // eslint-disable-next-line global-require
    const NextApp = require('./containers/app').default;
    render(renderApp(NextApp), rootEl);
  });
}

renderApp(App);
