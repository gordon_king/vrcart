import { createStore, applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import createHistory from 'history/createBrowserHistory';
import thunkMiddleware from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import { reducers, initialState } from '../reducers';

const rootReducer = combineReducers({
  ...reducers,
  router: routerReducer,
});

const persistConfig = {
  key: 'primary190302',
  storage,
  whitelist: [
    'settings',
  ],
};

const reducer = persistReducer(persistConfig, rootReducer);

export const history = createHistory();

export const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(
    applyMiddleware(
      routerMiddleware(history),
      thunkMiddleware,
    ),
  ),
);

export const persistor = persistStore(store);
