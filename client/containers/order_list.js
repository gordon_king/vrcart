import {
  Row, Col, Table, Button, ButtonGroup,
} from 'reactstrap';

import React, { Component } from 'react';
import accounting from 'accounting';

class OrderList extends Component {
  constructor(props) {
    super(props);
    this.handleRemoveClick = this.handleRemoveClick.bind(this);
    this.handleCompleteClick = this.handleCompleteClick.bind(this);
    this.handleCancelClick = this.handleCancelClick.bind(this);
  }

  getOrderlineDetails(productId, quantity, price) {
    const details = {
      productName: '-',
      orderlineTotal: 0,
    };
    const product = this.props.products.find(p => p.id === productId);
    if (product) {
      details.productName = product.name;
      details.orderlineTotal = price * quantity;
    }
    return details;
  }

  handleRemoveClick(e) {
    const value = e.currentTarget.getAttribute('data-id');
    if (this.props.handleOrderlineRemoveClick) {
      const orderlineId = Number.isNaN(parseInt(value, 10)) ? '' : parseInt(value, 10);
      if (orderlineId) { this.props.handleOrderlineRemoveClick(orderlineId); }
    }
  }

  handleCompleteClick() {
    if (this.props.handleOrderCompleteClick) {
      this.props.handleOrderCompleteClick();
    }
  }

  handleCancelClick() {
    if (this.props.handleOrderCancelClick) {
      this.props.handleOrderCancelClick();
    }
  }

  render() {
    const {
      id: orderId,
      status,
      total,
      orderlines,
    } = this.props.order;

    return (
      <Col>
        <Row>
          <Col sm={12}>
            <h3>{`Order #${orderId} - ${status}`}</h3>
          </Col>
        </Row>
        <Table hover>
          <thead>
            <tr>
              <th>Id</th>
              <th>Product</th>
              <th>Quantity</th>
              <th className="text-right">Unit Price</th>
              <th className="text-right">Total</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {
              orderlines
                .map((model) => {
                  const {
                    id: orderlineId,
                    productId,
                    quantity,
                    price,
                  } = model;
                  const {
                    productName,
                    orderlineTotal,
                  } = this.getOrderlineDetails(productId, quantity, price);
                  return (
                    <tr key={orderlineId}>
                      <td>{orderlineId}</td>
                      <td>{productName}</td>
                      <td>{quantity}</td>
                      <td className="text-right">{accounting.formatMoney(price)}</td>
                      <td className="text-right">{accounting.formatMoney(orderlineTotal)}</td>
                      <td>
                        <Button
                          data-id={orderlineId}
                          color="link"
                          size="sm"
                          onClick={this.handleRemoveClick}
                        >
                          remove
                        </Button>
                      </td>
                    </tr>
                  );
                })
            }
          </tbody>
          <tfoot>
            <tr>
              <th colSpan={4} className="text-right">Order Total:</th>
              <th className="text-right">{accounting.formatMoney(total)}</th>
              <th>&nbsp;</th>
            </tr>
          </tfoot>
        </Table>
        <ButtonGroup>
          <Button
            color="danger"
            onClick={this.handleCancelClick}
          >
            Cancel Order
          </Button>
          <Button
            onClick={this.handleCompleteClick}
          >
            Complete Order
          </Button>
        </ButtonGroup>
      </Col>
    );
  }
}

export default OrderList;
