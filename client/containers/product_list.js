import {
  Row, Col, Table, Button,
} from 'reactstrap';

import React, { Component } from 'react';
import accounting from 'accounting';

class ProductList extends Component {
  constructor(props) {
    super(props);
    this.handleAddClick = this.handleAddClick.bind(this);
  }

  handleAddClick(e) {
    const value = e.currentTarget.getAttribute('data-id');
    if (this.props.handleProductAddClick) {
      const productId = Number.isNaN(parseInt(value, 10)) ? '' : parseInt(value, 10);
      if (productId) { this.props.handleProductAddClick(productId); }
    }
  }

  render() {
    return (
      <Col>
        <Row>
          <Col sm={12}>
            <h3>Products</h3>
          </Col>
        </Row>
        <Table hover>
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th className="text-right">Price</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.products
                .map((model) => {
                  const {
                    id,
                    price,
                    name,
                  } = model;
                  return (
                    <tr key={id}>
                      <td>{id}</td>
                      <td>{name}</td>
                      <td className="text-right">{accounting.formatMoney(price)}</td>
                      <td>
                        <Button
                          data-id={id}
                          color="link"
                          size="sm"
                          onClick={this.handleAddClick}
                        >
                          add to order
                        </Button>
                      </td>
                    </tr>
                  );
                })
            }
          </tbody>
        </Table>
      </Col>
    );
  }
}

export default ProductList;
