import React from 'react';

import {
  Navbar,
  NavbarBrand,
} from 'reactstrap';

const Header = () => (
  <div>
    <Navbar color="light" light expand="md">
      <NavbarBrand href="/">
        <strong>Cart</strong>
      </NavbarBrand>
    </Navbar>
  </div>
);

export default Header;
