import {
  Container,
} from 'reactstrap';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { graphql } from 'react-apollo';

import _get from 'lodash/get';

import {
  mutationSet,
  mutationSuccess,
  mutationFailure,
} from '../actions/mutation_actions';

import {
  settingsSet,
} from '../actions/settings_actions';

import Loader from '../components/loader';
import ProductList from './product_list';
import OrderList from './order_list';

import orderCreateMutation from '../mutations/order_create_mutation';
import orderUpdateMutation from '../mutations/order_update_mutation';
import orderDeleteMutation from '../mutations/order_delete_mutation';
import orderlineCreateMutation from '../mutations/orderline_create_mutation';
import orderlineUpdateMutation from '../mutations/orderline_update_mutation';
import orderlineDeleteMutation from '../mutations/orderline_delete_mutation';

import orderQuery from '../queries/order_query';
import productsQuery from '../queries/products_query';

class CartShow extends Component {
  constructor(props) {
    super(props);
    this.handleOrderCompleteClick = this.handleOrderCompleteClick.bind(this);
    this.handleProductAddClick = this.handleProductAddClick.bind(this);
    this.handleOrderlineRemoveClick = this.handleOrderlineRemoveClick.bind(this);
    this.handleOrderCancelClick = this.handleOrderCancelClick.bind(this);
  }

  handleOrderlineRemoveClick(orderlineId) {
    const {
      orderlines,
    } = this.props.orderQuery.order;
    const orderline = orderlines.find(o => o.id === orderlineId);
    if (orderline) {
      let mutation;
      const mutationData = {
        variables: {
          id: orderlineId,
        },
      };
      const {
        quantity,
      } = orderline;
      if (quantity === 1) {
        mutation = this.props.orderlineDeleteMutation;
      } else {
        mutation = this.props.orderlineUpdateMutation;
        mutationData.variables.input = { quantity: quantity - 1 };
      }
      mutation(mutationData)
        .then(() => {
          this.props.mutationSuccess();
          this.props.orderQuery.refetch();
        })
        .catch(() => this.props.mutationFailure());
    }
  }

  handleProductAddClick(productId) {
    const product = this.props.productsQuery.products.find(p => p.id === productId);
    let mutation;
    if (this.props.orderQuery) {
      const {
        id: orderId,
        orderlines,
      } = this.props.orderQuery.order;
      const mutationData = {
        variables: {
          input: {
            productId,
            orderId,
            quantity: 1,
          },
        },
      };
      const orderline = orderlines.find(o => o.productId === productId);
      if (orderline) {
        mutation = this.props.orderlineUpdateMutation;
        const {
          id: orderlineId,
          quantity,
        } = orderline;
        mutationData.variables.id = orderlineId;
        mutationData.variables.input.quantity = quantity + 1;
      } else {
        mutation = this.props.orderlineCreateMutation;
        mutationData.variables.input.price = product.price;
      }
      mutation(mutationData)
        .then(() => {
          this.props.mutationSuccess();
          this.props.orderQuery.refetch();
        })
        .catch(() => this.props.mutationFailure());
    } else {
      this.props.orderCreateMutation({
        variables: {
          input: {
            status: 'open',
            orderlines: [
              {
                productId,
                quantity: 1,
                price: product.price,
              },
            ],
          },
        },
      })
        .then((resp) => {
          this.props.mutationSuccess();
          this.props.settingsSet({ orderId: resp.data.orderCreate.id });
        })
        .catch(() => this.props.mutationFailure());
    }
  }

  handleOrderCompleteClick() {
    this.props.mutationSet(false);
    const orderId = _get(this.props, 'orderQuery.order.id');
    if (orderId) {
      this.props.mutationSet(true);
      this.props.orderUpdateMutation({
        variables: {
          id: orderId,
          input: {
            status: 'complete',
          },
        },
      })
        .then(() => {
          this.props.mutationSuccess();
          this.props.settingsSet({ orderId: '' });
        })
        .catch(() => this.props.mutationFailure());
    }
  }

  handleOrderCancelClick() {
    this.props.mutationSet(false);
    const orderId = _get(this.props, 'orderQuery.order.id');
    if (orderId) {
      this.props.mutationSet(true);
      this.props.orderDeleteMutation({
        variables: {
          id: orderId,
        },
      })
        .then(() => {
          this.props.mutationSuccess();
          this.props.settingsSet({ orderId: '' });
        })
        .catch(() => this.props.mutationFailure());
    }
  }

  renderProductList() {
    return (
      <ProductList
        products={this.props.productsQuery.products}
        handleProductAddClick={this.handleProductAddClick}
      />
    );
  }

  renderOrderList() {
    return (
      <OrderList
        order={this.props.orderQuery.order}
        products={this.props.productsQuery.products}
        handleOrderlineRemoveClick={this.handleOrderlineRemoveClick}
        handleOrderCompleteClick={this.handleOrderCompleteClick}
        handleOrderCancelClick={this.handleOrderCancelClick}
      />
    );
  }

  renderOverlay() {
    if (
      this.props.settingsMutating
      || _get(this.props, 'productsQuery.loading', true)
      || (this.props.orderQuery && _get(this.props, 'orderQuery.loading', true))
    ) { return <Loader />; }
    return '';
  }

  renderData() {
    return (
      <div>
        {
          _get(this.props, 'productsQuery.loading', true) || this.renderProductList()
        }
        {
         _get(this.props, 'productsQuery.loading', true) || _get(this.props, 'orderQuery.loading', true) || this.renderOrderList()
        }
      </div>
    );
  }

  render() {
    return (
      <Container>
        { this.renderOverlay() }
        { this.renderData() }
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    settingsMutating: state.settings.mutating,
    settingsOrderId: state.settings.orderId,
  };
}

export default compose(
  connect(mapStateToProps, {
    mutationSet,
    mutationSuccess,
    mutationFailure,
    settingsSet,
  }),
  graphql(orderCreateMutation, {
    name: 'orderCreateMutation',
  }),
  graphql(orderUpdateMutation, {
    name: 'orderUpdateMutation',
  }),
  graphql(orderDeleteMutation, {
    name: 'orderDeleteMutation',
  }),
  graphql(orderlineCreateMutation, {
    name: 'orderlineCreateMutation',
  }),
  graphql(orderlineUpdateMutation, {
    name: 'orderlineUpdateMutation',
  }),
  graphql(orderlineDeleteMutation, {
    name: 'orderlineDeleteMutation',
  }),
  graphql(productsQuery, {
    name: 'productsQuery',
    options: () => ({ fetchPolicy: 'network-only' }),
  }),
  graphql(orderQuery, {
    name: 'orderQuery',
    skip: props => !props.settingsOrderId,
    options: props => (
      { variables: { id: props.settingsOrderId }, fetchPolicy: 'network-only' }
    ),
  }),

)(CartShow);
