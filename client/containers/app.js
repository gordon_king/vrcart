import { Container } from 'reactstrap';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Route, Redirect, Switch } from 'react-router-dom';
import Dimensions from 'react-dimensions';

import ErrorBoundaryApp from '../components/error_boundary_app';

import CartShow from './cart_show';
import Header from './header';

// bootstrap
require('bootstrap/dist/css/bootstrap.css');

const App = ({ containerHeight }) => (
  <ErrorBoundaryApp containerHeight={containerHeight}>
    <div>
      <Header />
      <Container fluid>
        <Switch>
          <Redirect exact from="/" to="/cart" />
          <Route path="/cart" component={CartShow} />
        </Switch>
      </Container>
    </div>
  </ErrorBoundaryApp>
);

App.propTypes = {
  containerHeight: PropTypes.number.isRequired,
};

function mapStateToProps() {
  return {};
}

export default compose(
  Dimensions(),
  connect(mapStateToProps, {}),
)(App);
