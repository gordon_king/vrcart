import {
  GraphQLObjectType,
  GraphQLInt,
  GraphQLFloat,
  GraphQLString,
} from 'graphql';

export default new GraphQLObjectType({
  name: 'OrderlineType',
  fields: () => ({
    id: { type: GraphQLInt },
    orderId: { type: GraphQLInt },
    productId: { type: GraphQLInt },
    quantity: { type: GraphQLFloat },
    price: { type: GraphQLFloat },
    total: { type: GraphQLFloat },
    productName: { type: GraphQLString },
  }),
});
