import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLFloat,
  GraphQLList,
} from 'graphql';

import OrderlineType from './orderline_type';

export default new GraphQLObjectType({
  name: 'OrderType',
  fields: () => ({
    id: { type: GraphQLInt },
    status: { type: GraphQLString },
    total: { type: GraphQLFloat },
    orderlines: { type: new GraphQLList(OrderlineType) },
  }),
});
