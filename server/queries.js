import {
  GraphQLObjectType,
  GraphQLList,
  GraphQLID,
  GraphQLNonNull,
} from 'graphql';

import ProductType from './types/product_type';
import OrderType from './types/order_type';

import { Product, Order, Orderline } from './store';

const Queries = new GraphQLObjectType({
  name: 'Queries',
  fields: {
    products: {
      type: new GraphQLList(ProductType),
      async resolve() {
        return Product.findAll();
      },
    },
    order: {
      type: OrderType,
      args: { id: { type: new GraphQLNonNull(GraphQLID) } },
      async resolve(parentValue, args) {
        return Order.find({
          where: args,
          include: [{
            model: Orderline,
            as: 'orderlines',
          }],
        });
      },
    },
  },
});

export default Queries;
