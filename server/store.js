import Sequelize from 'sequelize';

const productSeed = [
  {
    name: 'Sledgehammer',
    price: 125.76,
  }, {
    name: 'Axe',
    price: 190.51,
  }, {
    name: 'Bandsaw',
    price: 562.14,
  }, {
    name: 'Chisel',
    price: 13.9,
  }, {
    name: 'Hacksaw',
    price: 19.45,
  },
];

const db = new Sequelize('cart', null, null, {
  dialect: 'sqlite',
  storage: './cart.sqlite',
});

const ProductModel = db.define('product', {
  name: { type: Sequelize.STRING },
  price: { type: Sequelize.DECIMAL(10, 2) },
});

const OrderModel = db.define('order', {
  status: { type: Sequelize.STRING },
  total: {
    type: Sequelize.VIRTUAL,
    get() {
      return this.getOrderlines()
        .then(orderlines => (
          orderlines.reduce((total, orderline) => {
            const {
              price,
              quantity,
            } = orderline.dataValues;
            return Math.floor((total + (price * quantity)) * 100) / 100;
          }, 0)
        ));
    },
  },
});

const OrderlineModel = db.define('orderline', {
  quantity: { type: Sequelize.INTEGER },
  price: { type: Sequelize.DECIMAL(10, 2) },
  total: {
    type: Sequelize.VIRTUAL,
    get() {
      return Math.floor((this.getDataValue('quantity') * this.getDataValue('price')) * 100) / 100;
    },
  },
  productName: {
    type: Sequelize.VIRTUAL,
    get() {
      return this.getProduct()
        .then(product => product.getDataValue('name'));
    },
  },
});

OrderModel.hasMany(OrderlineModel, { onDelete: 'cascade' });
OrderlineModel.belongsTo(OrderModel);

ProductModel.hasMany(OrderlineModel);
OrderlineModel.belongsTo(ProductModel);

const seed = () => {
  db.sync({ force: true })
    .then(() => {
      const productPromises = productSeed.map(product => ProductModel.create(product));
      Promise.all(productPromises)
        .then((products) => {
          OrderModel.create({
            status: 'open',
          })
            .then((order) => {
              order.createOrderline({
                productId: products[3].id,
                price: products[3].price,
                quantity: 2,
              });
              order.createOrderline({
                productId: products[0].id,
                price: products[0].price,
                quantity: 1,
              });
            });
        });
    });
};

ProductModel.count()
  .then((resp) => {
    if (resp === 0) {
      console.log('seeding');
      seed();
    } else {
      console.log('seeded');
    }
  })
  .catch(() => {
    console.log('seeding empty');
    seed();
  });

const Product = db.models.product;
const Order = db.models.order;
const Orderline = db.models.orderline;

export { Product, Order, Orderline };
