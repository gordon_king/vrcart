import {
  GraphQLObjectType,
  GraphQLID,
} from 'graphql';

import OrderType from './types/order_type';
import OrderInput from './inputs/order_input';
import OrderlineType from './types/orderline_type';
import OrderlineInput from './inputs/orderline_input';

import { Order as OrderModel, Orderline as OrderlineModel } from './store';

import { DbMutationError } from './errors';

const handleDbCatch = (res) => {
  if (res.name) {
    throw new DbMutationError({
      data: {
        statusCode: 400,
        statusText: 'Bad Request Error',
        message: res.name,
        errors: res.original,
      },
    });
  } else {
    return res;
  }
};

const Mutations = new GraphQLObjectType({
  name: 'Mutations',
  fields: {
    orderCreate: {
      type: OrderType,
      args: {
        input: { type: OrderInput },
      },
      resolve(parentValue, { input }) {
        return OrderModel.create(input)
          .then((order) => {
            const orderlinePromises = input.orderlines.map(orderline => (
              order.createOrderline(orderline)
            ));
            return Promise.all(orderlinePromises)
              .then(() => OrderModel.find({
                where: { id: order.dataValues.id },
              }))
              .catch(err => handleDbCatch(err));
          })
          .catch(err => handleDbCatch(err));
      },
    },
    orderUpdate: {
      type: OrderType,
      args: {
        id: { type: GraphQLID },
        input: { type: OrderInput },
      },
      resolve(parentValue, { id, input }) {
        return OrderModel.update(input, { where: { id } })
          .then(() => OrderModel.find({
            where: { id },
          }))
          .catch(err => handleDbCatch(err));
      },
    },
    orderDelete: {
      type: OrderType,
      args: {
        id: { type: GraphQLID },
      },
      resolve(parentValue, { id }) {
        return OrderModel.destroy({ where: { id } })
          .then(() => ({ id }))
          .catch(err => handleDbCatch(err));
      },
    },
    orderlineCreate: {
      type: OrderlineType,
      args: {
        input: { type: OrderlineInput },
      },
      resolve(parentValue, { input }) {
        return OrderlineModel.create(input)
          .then(resp => OrderlineModel.find({
            where: { id: resp.dataValues.id },
          }))
          .catch(err => handleDbCatch(err));
      },
    },
    orderlineUpdate: {
      type: OrderlineType,
      args: {
        id: { type: GraphQLID },
        input: { type: OrderlineInput },
      },
      resolve(parentValue, { id, input }) {
        return OrderlineModel.update(input, { where: { id } })
          .then(() => OrderlineModel.find({
            where: { id },
          }))
          .catch(err => handleDbCatch(err));
      },
    },
    orderlineDelete: {
      type: OrderlineType,
      args: {
        id: { type: GraphQLID },
      },
      resolve(parentValue, { id }) {
        return OrderlineModel.destroy({ where: { id } })
          .then(() => ({ id }))
          .catch(err => handleDbCatch(err));
      },
    },
  },
});

export default Mutations;
