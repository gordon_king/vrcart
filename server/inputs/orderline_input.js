import {
  GraphQLInputObjectType,
  GraphQLID,
  GraphQLInt,
  GraphQLFloat,
} from 'graphql';

export default new GraphQLInputObjectType({
  name: 'OrderlineInput',
  fields: {
    id: { type: GraphQLID },
    orderId: { type: GraphQLInt },
    productId: { type: GraphQLInt },
    quantity: { type: GraphQLInt },
    price: { type: GraphQLFloat },
  },
});
