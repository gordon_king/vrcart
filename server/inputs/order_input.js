import {
  GraphQLInputObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLList,
} from 'graphql';

import OrderlineInputType from './orderline_input';

export default new GraphQLInputObjectType({
  name: 'OrderInput',
  fields: {
    id: { type: GraphQLID },
    status: { type: GraphQLString },
    orderlines: { type: new GraphQLList(OrderlineInputType) },
  },
});
