/* eslint-disable */
import exitHook from 'exit-hook';
import path from 'path';
import express from 'express';
import bearerToken from 'express-bearer-token';

import { ApolloServer } from 'apollo-server-express';
import { formatError } from 'apollo-errors';

import schema from './schema';

const nodeEnv = process.env.NODE_ENV;
const isDeveloping = nodeEnv !== 'production';
const port = process.env.PORT || 1337;
const app = express();
const graphqlPath = '/graphql';

function logger(req, res, next) {
  // eslint-disable-next-line no-console
  console.log(new Date(), req.method, req.url, req.query);
  next();
}

if (isDeveloping) {
  app.use(logger);
}

app.use(bearerToken());

const server = new ApolloServer({
  formatError,
  schema,
  context: () => ({
    token: 'abc',
  }),
});

server.applyMiddleware({
  app,
  path: graphqlPath,
});

if (isDeveloping) {
  // eslint-disable-next-line no-console
  console.log('express is developing');
  const webpack = require('webpack');
  const config = require('../webpack/webpack.dev.js');
  const compiler = webpack(config());

  const webpackDevMiddleware = require('webpack-dev-middleware')(
    compiler,
    config.devServer,
  );

  const webpackHotMiddlware = require('webpack-hot-middleware')(
    compiler,
    config.devServer,
  );

  const historyApiFallback = require('connect-history-api-fallback');

  app.use(historyApiFallback({
    verbose: false,
  }));

  app.use(webpackDevMiddleware);
  app.use(webpackHotMiddlware);
} else {
  console.log('express is productioning');
  app.use(express.static(path.join(__dirname, '../dist/')));
  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../dist', 'index.html'));
  });
}

const expressStaticGzip = require('express-static-gzip');

app.use(
  expressStaticGzip('dist', {
    enableBrotli: true,
  }),
);

const webServer = app.listen(port, (error) => {
  if (error) {
    console.log(error);
  }
  console.info('==> Listening on port %s.  Node environment is %s', port, nodeEnv);
});

exitHook(() => {
  console.log('Received kill signal. Shutting down');
});
