import { createError } from 'apollo-errors';

export const DbMutationError = createError('DbMutationError', {
  message: 'Something went wrong db server',
  options: { showLocations: true, showPath: true },
});

export const DbQueryError = createError('DbQueryError', {
  message: 'Something went wrong',
  options: { showLocations: true, showPath: true },
});

