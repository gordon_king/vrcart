const path = require('path')
const webpack = require('webpack')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const TodoWebpackPlugin = require('todo-webpack-plugin')
const MomentLocalesPlugin = require('moment-locales-webpack-plugin')
const { parsed } = require('dotenv').config()

module.exports = () => {
  return {
    name: 'development-client',
    mode: 'development',
    entry: {
      main: [
        'react-hot-loader/patch',
        '@babel/runtime/regenerator',
        '@babel/register',
        'webpack-hot-middleware/client?reload=true',
        path.resolve(__dirname, '../client', 'index.js')
      ]
    },
    output: {
      filename: '[name]-bundle.js',
      path: path.resolve(__dirname, '../dist'),
      publicPath: '/'
    },
    resolve: {
      extensions: [
        '.js',
        '.gql',
        '.graphql'
      ]
    },
    devServer: {
      contentBase: 'dist',
      overlay: true,
      stats: {
        colors: true
      }
    },
    devtool: 'eval-source-map',
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader'
            },
            {
              loader: 'eslint-loader'
            }
          ]
        },
        {
          test: /\.css$/,
          use: [
            {
              loader: 'style-loader'
            },
            { loader: 'css-loader' }
          ]
        },
        {
          test: /\.scss$/,
          use: [
            {
              loader: 'style-loader'
            },
            {
              loader: 'css-loader'
            },
            {
              loader: 'sass-loader'
            }
          ]
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 8192
              }
            }
          ]
        },
        {
          test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
          use: [
            {
              loader: 'file-loader'
            }
          ]
        },
        {
          test: /\.(graphql|gql)$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'graphql-tag/loader'
            }
          ]
        }
      ]
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('development'),
          // AUTH_API_URL: JSON.stringify(process.env.AUTH_API_URL),
          // AUTH_APP_URL: JSON.stringify(process.env.AUTH_APP_URL),
          // AUTH_CLIENT_ID: JSON.stringify(process.env.AUTH_CLIENT_ID),
          // AUTH_CLIENT_SECRET: JSON.stringify(process.env.AUTH_CLIENT_SECRET),
          APP_URL: JSON.stringify(process.env.APP_URL)
        }
      }),
      new TodoWebpackPlugin({
        console: false
      }),
      new HTMLWebpackPlugin({
        template: path.resolve(__dirname, '../client', 'index.ejs'),
        inject: true,
        node_env: JSON.stringify('development')
      }),
      new MomentLocalesPlugin({
        localesToKeep: ['en-nz']
      }),
      new BundleAnalyzerPlugin({
        generateStatsFile: true,
        openAnalyzer: false
      })
    ]
  }
}
